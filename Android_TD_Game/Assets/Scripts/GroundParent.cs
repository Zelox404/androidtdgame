﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundParent : MonoBehaviour
{

    private GameObject nodechild;
    private Renderer[] r_nodechild;
    public Color startColor;
    void Start()
    {
        r_nodechild = new Renderer[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            nodechild = transform.GetChild(i).gameObject;
            r_nodechild[i] = nodechild.GetComponentInChildren<Renderer>();
        }
        startColor = r_nodechild[0].material.color;
    }
    public void ClickUpdate()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            nodechild = transform.GetChild(i).gameObject;
            r_nodechild[i] = nodechild.GetComponentInChildren<Renderer>();
            r_nodechild[i].material.color = startColor;
        }
    }
}
