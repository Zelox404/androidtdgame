﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform target;

    [Header("Bullet & Impact Stats")]
    public float speed = 50f;
    public float effectDuration = 2f;
    public GameObject impactEffect;

    
    // Update is called once per frame
    void Update()
    {
        if(target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;
        if(dir.magnitude <= distanceThisFrame)
        {
            TargetHit();
            return;
        }
        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
    }

    public void TargetSeek(Transform targetS)
    {
        target = targetS;
    }
    public void TargetHit()
    {
        GameObject effectInst = Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectInst, effectDuration);

        Destroy(target.gameObject);
        Destroy(gameObject);
        return;
    }
}
