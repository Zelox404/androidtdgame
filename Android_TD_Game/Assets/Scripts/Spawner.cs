﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    public Transform enemyPrefab;
    public Transform spawnpoint;
    [Header("Stats")]
    public float distanceBetweenEnemies = 0.5f;
    [Header("Time Attributes")]
    public float waveTimer = 5f;
    private float countdown = 2f;
    public Text waveCountdownText;
    private int waveIndex = 1;
    private int waveAdds = 0;
    [Header("Difficulty & Spawning")]
    public int enemyStartcount = 10;


    void Update()
    {
        if(countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = waveTimer;
        }
        countdown -= Time.deltaTime;
        waveCountdownText.text = Mathf.Round(countdown).ToString();
    }
    IEnumerator SpawnWave()
    {
        for (int i = 0; i < enemyStartcount + waveAdds; i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(distanceBetweenEnemies);
        }
        waveIndex ++;
        waveAdds = waveIndex ^ 2;
    }
    void SpawnEnemy()
    {
        Instantiate(enemyPrefab, spawnpoint.position, spawnpoint.rotation);
    }

}
