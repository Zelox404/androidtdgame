﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buildmanager : MonoBehaviour
{
    

    public static Buildmanager instance;
    

    private void Awake()
    {
        if(instance != null)
        {
            Debug.Log("More than one Buildmanager in Scene!");
            return;
        }

        instance = this;
    }
    public GameObject standardTurretPrefab;

    private void Start()
    {
        turretToBuild = standardTurretPrefab;
    }

    private GameObject turretToBuild;
    public GameObject getTurretToBuild()
    {
        return turretToBuild;
    }


}
