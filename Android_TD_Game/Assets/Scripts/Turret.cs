﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{

    

    [Header("Turret Data")]
    public Transform target;
    public Transform RotateObject;
    public float updateTargetInterval = 0.5f;
    public string enemyTag = "Enemy";
    public GameObject bulletPrefab;
    public Transform firePoint;

    [Header("Turret Attributes")]
    public float range = 8f;
    public float rotationSpeed = 5f;
    public float fireRate = 1f;

    private float fireCountdown = 0f;

    void Start()
    {
        InvokeRepeating("UpdateTarget",0f,updateTargetInterval);
    }

    void Update()
    {
        if(target == null)
        {
            return;
        }
        //Rotation of the turret
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(RotateObject.rotation,lookRotation,Time.deltaTime*rotationSpeed).eulerAngles;
        RotateObject.rotation = Quaternion.Euler(0f, rotation.y, 0f);

        if(fireCountdown <= 0)
        {
            Shoot();
            fireCountdown = 1f / fireRate;
        }
        fireCountdown -= Time.deltaTime;
    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            //find every enemy and check each distances to the turret to determine the shortest distance
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        }
        else target = null;
    }

    void Shoot()
    {
        GameObject bulletGObject = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGObject.GetComponent<Bullet>();

        if(bullet != null)
        {
            bullet.TargetSeek(target);
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
