﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    public Vector3 positionOffset;
    public Vector3 rotationOffset;
    public Color clickColor;
    private Renderer rend;
    public GroundParent parent;
    public GameObject turret;
    void Start()
    {
        
        rend = GetComponent<Renderer>();
    }
    private void OnMouseDown()
    {
        parent.ClickUpdate();
        rend.material.color = clickColor;

        if(turret != null)
        {
            Debug.Log("Can´t build there! - implement as global text");
            return;
        }

        GameObject turretToBuild = Buildmanager.instance.getTurretToBuild();
        turret = Instantiate(turretToBuild, transform.position + positionOffset, transform.rotation);
    }


}
